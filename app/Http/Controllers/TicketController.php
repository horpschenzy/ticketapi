<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use App\Models\Ticket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::with('replies')->get();
        return response()->json(['message'=> $tickets ],200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'department' => 'required',
            'service' => 'required',
            'priority' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return response()->json(["errors" => $v->messages()->first()], 422);
        }

        $data = $request->only(['name', 'email', 'department', 'service', 'priority','subject','message']);
        $data['status'] = 'open';
        $data['ticket_code'] = '#UAM-'.mt_rand(100, 999).'-'.mt_rand(1000,9999).'-'.str_replace(' ','-',$request->name);
        $data['user_id'] = Auth::id();
        $ticket = new Ticket($data);
        if($ticket->save()){
            return response()->json(['message'=> 'Ticket Added Successful'],201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function reply(Request $request, $ticket_id)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ];
        $v = Validator::make($request->all(), $rules);
        if ($v->fails()) {
            return response()->json(["errors" => $v->messages()->first()], 422);
        }
        $ticket = Ticket::where('id', $ticket_id);
        if($ticket->first()){
            $data = $request->only(['name', 'email', 'message']);
            $data['status'] = 'replied';
            $data['ticket_id'] = $ticket_id;
            $reply = new Reply($data);
            if($reply->save()){
                return response()->json(['message'=> 'Replies Added Successfully'],201);
            }
        }else{
            return response()->json(['message'=> 'Ticket not found'],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit($ticket)
    {
        $ticket = Ticket::where('id', $ticket)->first();
        return response()->json(['message'=> $ticket ],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticket $ticket)
    {
        //
    }
}
