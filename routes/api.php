<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login',[App\Http\Controllers\AuthController::class, 'login']);
Route::post('register',[App\Http\Controllers\AuthController::class, 'store']);
Route::get('logout',[App\Http\Controllers\AuthController::class, 'logout']);
Route::group(['middleware' => ['auth:api']], function() {
    Route::post('add/ticket',[App\Http\Controllers\TicketController::class, 'store']);
    Route::post('reply/ticket/{id}',[App\Http\Controllers\TicketController::class, 'reply']);
    Route::get('ticket/{id}',[App\Http\Controllers\TicketController::class, 'edit']);
    Route::get('view/tickets',[App\Http\Controllers\TicketController::class, 'index']);
});

