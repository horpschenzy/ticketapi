<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('ticket_code', 255);
            $table->string('name', 255);
            $table->string('email', 255);
            $table->string('department', 255);
            $table->string('service', 255);
            $table->string('priority', 255);
            $table->string('subject', 255);
            $table->string('message', 255);
            $table->unsignedBigInteger('user_id');
            $table->enum('status', ['replied', 'open','close'])->default('open');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
